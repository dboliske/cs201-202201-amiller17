/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/30/2022 */

package exams.second;

public class Circle extends Polygon{
	
	private double radius;

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public boolean validRadius(double radius) {
		if(this.radius<0) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "The circle " + super.getName() + " has an area of " + area() + " and a perimeter of " + perimeter() + ".";
	}
	
	@Override
	public double area() {
		return Math.PI * radius * radius;
	}
	
	@Override
	public double perimeter() {
		return Math.PI * 2 * radius;
	}

}
