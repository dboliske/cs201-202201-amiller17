/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/30/2022 */

package exams.second;

public class Polygon {

	protected String name;
	
	public Polygon() {
		name = null;
	}

	public Polygon(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "The polygon has the name " + name + ".";
	}
	
	public double area() {
		return 0.00;
	}
	
	public double perimeter() {
		return 0.00;
	}
	
}
