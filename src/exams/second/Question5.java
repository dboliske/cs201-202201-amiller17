/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/30/2022 */

package exams.second;


import java.util.Scanner;

public class Question5 {

	public static void main(String[] args) {
		double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		boolean done = false;
		Scanner userIn = new Scanner(System.in);
		do {
			System.out.println("Enter the desired value.");
			try {
				double desired = Double.parseDouble(userIn.nextLine());
				System.out.println(desired);
				int index = jumpSearch(numbers, desired, (int) Math.sqrt(numbers.length)-1, 0);
				if(index == -1) {
					System.out.println("Unable to find the desired number.");
					done = true;
				}
				else {
					System.out.println("The desired number has an index of " + index + ".");
					done = true;
				}
				
			}
			catch(Exception e) {
				System.out.println("Please enter a number. \n");
			}
		}
		while(!done);
		userIn.close();
	}
	
	public static int jumpSearch(double[] array, double desired, int size, int last) {
		int jumpSize = (int) Math.sqrt(size);
		if(last < array.length && desired >array[last]) {
			last += jumpSize;
			return jumpSearch(array, desired, size, last);
		}
		else {
			for(int search = last-jumpSize+1; search<=last && search<array.length; search++) {
				if(desired == array[search]) {
					return search;
				}
			}
		}
		return -1;
	}

}
