/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/30/2022 */

package exams.second;

public class Rectangle extends Polygon {
	
	private double height;
	private double width;
	
	public Rectangle() {
		super();
		height = 1;
		width = 1;
	}
	
	public Rectangle(String name, double height, double width) {
		this.name = name;
		this.height = height;
		this.width = width;
	}
	
	public double getHeight() {
		return height;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
	public boolean validHeight() {
		if(this.height<0) {
			return false;
		}
		return true;
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
	
	public boolean validWidth() {
		if(this.width<0) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "The rectangle " + super.getName() + " has an area of " + area() + " and a perimeter of " + perimeter() + ".";
	}
	
	@Override
	public double area() {
		return width * height;
	}
	
	@Override
	public double perimeter() {
		return 2 * width * height;
	}
}
