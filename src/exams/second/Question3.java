/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/30/2022 */

package exams.second;


import java.util.ArrayList;
import java.util.Scanner;

public class Question3 {

	public static void main(String[] args) {
		ArrayList<Double> numbers = new ArrayList<Double>();
		boolean done = false;
		Scanner userIn = new Scanner(System.in);
		do {
			System.out.println("Enter a number? Y/N.");
			String userYN = userIn.nextLine();
			switch(userYN.toLowerCase()) {
			case "y":
				System.out.println("Enter your number.");
				try {
					double userNum = Double.parseDouble(userIn.nextLine());
					numbers.add(userNum);
				}
				catch(Exception e) {
					System.out.println("No valid number detected.");	
				}
				break;
			case "n":
				System.out.println("Determining maximum and minimum of entered numbers.");
				done = true;
				break;
			default:
				System.out.println("Please enter a \"Y\" or an \"N\"");
				break;
			}
		}
		while(!done);
		if(numbers.size()==0) {
			System.out.println("No values entered. There is no maximum or minimum.");
		}
		else if(numbers.size()==1) {
			System.out.println("One value entered. The maximum and minimum are both " + numbers.get(0) + ".");
		}
		else {
			double minimum = numbers.get(0);
			double maximum = numbers.get(1);
			for(Double d: numbers) {
				if(d>maximum) {
					maximum = d;
				}
				else if(d<minimum) {
					minimum = d;
				}
			}
			System.out.println("The maximum of the entered values is " + maximum + " and the minimum is " + minimum + ".");
		}
		userIn.close();
	}

}
