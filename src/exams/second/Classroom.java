/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/30/2022 */

package exams.second;

public class Classroom {
	
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		building = null;
		roomNumber = null;
		seats = 1;
	}
	
	public Classroom(String building, String roomNumber, int seats) {
		this.building = building;
		this.roomNumber = roomNumber;
		this.seats = seats;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	
	public boolean validSeats(int seats) {
		if(this.seats<0) {
			return false;
		}
		return true;
	}
	
	public String toString() {
		return "The classroom is in " + building + ", room " + roomNumber + ", with " + seats + " seats.";
	}
	
}
