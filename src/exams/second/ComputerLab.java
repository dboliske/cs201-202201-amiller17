/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/30/2022 */

package exams.second;

public class ComputerLab extends Classroom{
	
	private boolean computers;
	
	public ComputerLab() {
		super();
		computers = true;
	}
	
	public ComputerLab(String building, String roomNumber, int seats, boolean computers) {
		super(building, roomNumber, seats);
		this.computers = computers;
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	@Override
	public String toString() {
		if(!computers) {
			return "The computer lab is in " + super.getBuilding() + ", room " + super.getRoomNumber() + ", with " + super.getSeats() + " seats. It does not have computers.";
		}
		return "The computer lab is in " + super.getBuilding() + ", room " + super.getRoomNumber() + ", with " + super.getSeats() + " seats. It does have computers.";
	}
}
