package exams.first;

public class Pet {
	private String name;
	private int age;
	
	public Pet() {
		name = "Stan";
		age = 1;
	}
	
	public Pet(String nameEnt, int ageEnt) {
		name = nameEnt;
		age = ageEnt;
	}
	
	public void setName(String nameEnt) {
		this.name = nameEnt;
	}
	
	public void setAge(int ageEnt) {
		this.age = ageEnt;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Pet userPet) {
		if (this.name == userPet.getName() && this.age == userPet.getAge()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public String toString() {
		return "The pet's name is " + name + " and they are " + age + " year(s) old.";
	}
	
}
