package exams.first;
import java.util.Scanner;
public class Question2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter an integer value.");
		String userIn = input.nextLine();
		int userVal = Integer.parseInt(userIn);
		if(userVal%2 == 0 && userVal%3 == 0) {
			System.out.println("foobar");
		}
		else if(userVal%2 == 0 && userVal%3 !=0) {
			System.out.println("foo");
		}
		else if(userVal%2 != 0 && userVal%3 == 0) {
			System.out.println("bar");
		}
		input.close();
	}

}
