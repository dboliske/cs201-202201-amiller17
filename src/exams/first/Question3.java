package exams.first;
import java.util.Scanner;
public class Question3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a posistive integer.");
		String userIn = input.nextLine();
		int userVal = Integer.parseInt(userIn);
		for(int i = 1; i<=userVal; i++) {
			for(int k = 1; k<=i; k++) {
				System.out.print("*  ");
			}
			System.out.println("\n");
		}
		input.close();
	}

}
