package exams.first;
import java.util.Scanner;
public class Question4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String userIn[] = {"1", "1", "1", "1", "1"};
		for(int i = 0; i<5; i++) {
			System.out.println("Enter a word.");
			userIn[i]= input.nextLine();
		}
		for(int j = 0; j<5; j++) {
			if(userIn[j] == userIn[0]) {
				System.out.println("The word at position " + j + "is the same as the first word.");
			}
			else if(userIn[j] == userIn[1]) {
				System.out.println("The word at position " + j + "is the same as the second word.");
			}
			else if(userIn[j] == userIn[2]) {
				System.out.println("The word at position " + j + "is the same as the third word.");
			}
			else if(userIn[j] == userIn[3]) {
				System.out.println("The word at position " + j + "is the same as the fourth word.");
			}
			else if(userIn[j] == userIn[4]) {
				System.out.println("The word at position " + j + "is the same as the fifth word.");
			}
		}
		input.close();
	}

}
