# Midterm Exam

## Total

84/100

## Break Down

1. Data Types:                  10/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               0/5
    - Results:                  0/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  20/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               5/5
4. Arrays:                      20/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  5/5
5. Objects:                     14/20
    - Variables:                5/5
    - Constructors:             3/5
    - Accessors and Mutators:   3/5
    - toString and equals:      3/5

## Comments

1. Program does not convert an integer to an character and does not print out the character.
2. Good
3. Good
4. Good
5. Non-default constructor and `setAge` do not validate `age` before assigning it, the `equals` method does not follow the UML diagram, and the `equals` method incorrectly compares the `names`.
