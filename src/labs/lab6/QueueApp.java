/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 3/23/2022 */

package labs.lab6;

import java.util.Scanner;

public class QueueApp {

	public static void main(String[] args) {
		QueueList queue = new QueueList();
		boolean done = false;
		Scanner userIn = new Scanner(System.in);
		String userAns;
		while(!done) {
			System.out.println("Enter the integer value of your option.");
			System.out.println("1. Add customer to queue." + "\n" + "2. Help customer." + "\n" + "3. Exit.");
			userAns = userIn.nextLine();
			switch(userAns) {
			case "1": case "1.":
			{
				System.out.println("Enter the customer's name.");
				String custName = userIn.nextLine();
				queue.addCustomer(custName);
				break;
			}
			case "2": case "2.":
			{
				
				queue.helpCustomer();
				break;
			}
			case "3": case "3.":
			{
				done = true;
				break;
			}
			default:
			{
				System.out.println("Enter a valid integer value.");
				break;
			}
			}
		}
		userIn.close();
		System.out.println("Program Ended.");
	}

}
