/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 3/23/2022 */

package labs.lab6;

public class QueueList {
	
	private int size;
	private String[] queue;
	
	public QueueList() {
		size = 0;
		queue = new String[0];
		
	}
	
	public  void addCustomer(String s) {
		String[] temp = new String[queue.length + 1];
		for( int i= 0; i<queue.length; i++) {
			temp[i] = queue[i];
		}
		queue = temp;
		temp = null;
		queue[size] = s;
		size++;
		System.out.println("The customer's position is " + size + ".");
	}
	
	public void helpCustomer() {
		System.out.println(queue[0]);
		if(size != 0) {
			for(int k = 0; k<size-1; k++) {
				String[] tempDown = new String[queue.length - 1];
				tempDown[k] = queue[k+1];
				queue = tempDown;
				tempDown = null;
			}
		}
		else {
			System.out.println("No customers to help.");
		}
		size--;
	}

}
