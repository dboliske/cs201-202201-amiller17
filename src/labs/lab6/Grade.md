# Lab 6

## Total

19/20

## Break Down

Deli Queue Application

- Loops to display options          2/2
- Adds customer to queue            6/6
- Removes customer from queue       5/6
- Use ArrayLists to store customers 6/6

## Comments
Index goes out of bound when deleting