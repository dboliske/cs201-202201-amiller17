package labs.lab1;
import java.util.Scanner;
public class WoodBox {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the length, width, and depth of your box.");
		double l = Double.parseDouble(input.nextLine());
		double w = Double.parseDouble(input.nextLine());
		double d = Double.parseDouble(input.nextLine());
		double SA = 2*l*w + 2*w*d + 2*l*d;
		System.out.println("The total square footage required to make the box is " + SA + " square feet of wood.");
		input.close();
	}
}

/* Test with 3 numbers per variable.
 * 1: L, W, D = 4.5, 5.6, 3.7 -> 125.14000000000001
 * 2: L, W, D = 2.2, 1.1, 2.2 -> 19.360000000000003 
 * 3: L, W, D = 1.8, 3.6, 5.4 -> 71.28
 */