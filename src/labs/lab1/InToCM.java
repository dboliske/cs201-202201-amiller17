package labs.lab1;
import java.util.Scanner;
public class InToCM {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of inches you'd like converted to centimeters.");
		double inches = Double.parseDouble(input.nextLine());
		System.out.println("Your inch value in centimeters is " + (inches*2.54) + " centimeters.");
		input.close();
	}
	
}
 /* Test 3 values for inches.
  * 1: 5.3 -> 13.462
  * 2: 1.0 -> 2.54
  * 3: 70 -> 177.8
  */