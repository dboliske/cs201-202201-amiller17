package labs.lab1;
import java.util.Scanner;
public class AgeAndHeight {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);	
		System.out.println("Enter your age.");
		int ageSon = Integer.parseInt(input.nextLine());
		System.out.println("Enter the age of your father.");
		int ageFather = Integer.parseInt(input.nextLine());
		System.out.println("The difference of your ages is " + (ageFather-ageSon) + ".");
		System.out.println("The double of your age is " + (ageSon*2)+".");
		System.out.println("Enter your height in inches, rounded to the nearest inch.");
		int height = Integer.parseInt(input.nextLine());
		System.out.println("Your height in centimeters is " + (height*2.54) + " centimeters.");
		int feet = height/12;
		System.out.println("Your height in feet and inches is " + feet + " foot " + (height%12) + " inches.");
		input.close();
		}
	
}
