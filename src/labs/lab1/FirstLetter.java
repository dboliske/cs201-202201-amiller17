package labs.lab1;
import java.util.Scanner;
public class FirstLetter {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter your first name?");
		char firstLetter = input.nextLine().charAt(0);
		System.out.println("The first letter of your first name is " + firstLetter + ".");
		input.close();
	}
}
