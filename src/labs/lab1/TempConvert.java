package labs.lab1;
import java.util.Scanner;
public class TempConvert {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a temperature in Fahrenheit.");
		float tempF = Float.parseFloat(input.nextLine());
		System.out.println(tempF + " degrees Fahrenheit is " + ((tempF-32)*5/9) + " degrees Celsius.");
		System.out.println("Enter a temperature in Celsius.");
		float tempC = Float.parseFloat(input.nextLine());
		System.out.println(tempC + " degrees Celsius is " + ((tempC*9/5)+32) + " degrees Fahrenheit.");
		input.close();
	}
}
 /* Test with three temperatures each, Fahrenheit & Celsius. 
  * Fahrenheit: -459.67 (Absolute Zero), 32 (Freezing Point Water), 212 (Boiling Point Water)
  * Celsius: -273.15 (Absolute Zero), 37 (Body Temperature), 356.7 (Boiling Point Mercury)
  * Conversion Results F: -459.67->-273.15002 32->0.0 212->100.0
  * Conversion Results C:-273.15->-459.66998 37->98.6 356.7->674.06
  */