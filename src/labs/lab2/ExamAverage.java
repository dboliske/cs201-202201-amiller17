/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 1/31/2022 */

package labs.lab2;

import java.util.Scanner;

public class ExamAverage {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double gradeTotal = 0;
		int inputsTotal = 0;
		System.out.println("Enter the exam grades. When done, enter the value -1.");
		double gradeIn = Double.parseDouble(input.nextLine());
		while(gradeIn>=0)
		{
			gradeTotal = gradeTotal + gradeIn;
			inputsTotal++;
			gradeIn = Double.parseDouble(input.nextLine());
		}
		if(inputsTotal==0)
		{
			System.out.println("No exam grades entered.");
		}
		else
		{
			System.out.println("The grade average for the " + inputsTotal + " grades is equal to " + (gradeTotal/inputsTotal) + ".");
		}
		input.close();
	}

}

/* Tested 4 grades: 90.2, 89.4, 96.7, 99. Average: 93.825
 * Tested 6 grades: 98.7, 89.2, 99.5, 86.5, 93.5, 94.6. Average: 93.66666666666667
 * Test 0 grades - "No exam grades entered."
 * 
 * */