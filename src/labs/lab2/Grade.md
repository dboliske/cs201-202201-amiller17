# Lab 2

## Total

20/20

## Break Down

* Exercise 1    6/6
* Exercise 2    6/6
* Exercise 3    6/6
* Documentation 2/2

## Comments
- Please read exercises carefully, ex3. should terminate on input "4" not "Exit"