/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 1/31/2022 */

package labs.lab2;

import java.util.Scanner;

public class ConsoleOptions {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean Done = false;
		double inOne;
		double inTwo;
		double sum;
		double product;
		while(!Done) 
		{
			System.out.println("Enter one of the following options.");
			System.out.println("Say Hello" + "\n" + "Addition" + "\n" + "Multiplication" + "\n" + "Exit");
			String userIn = input.nextLine();
			switch(userIn.toLowerCase())
			{
			case "say hello":
				System.out.println("Hello" + "\n");
				break;
			case "addition":
				System.out.println("Enter two values to add.");
				inOne = Double.parseDouble(input.nextLine());
				inTwo = Double.parseDouble(input.nextLine());
				sum = inOne + inTwo;
				System.out.println("The sum of the two values is " + sum + "." + "\n");
				break;
			case "multiplication":
				System.out.println("Enter two values to multiply.");
				inOne = Double.parseDouble(input.nextLine());
				inTwo = Double.parseDouble(input.nextLine());
				product = inOne * inTwo;
				System.out.println("The product of the two values is " + product + "." + "\n");
				break;
			case "exit":
				Done = true;
				break;
			default:
				System.out.println("That is not a valid option. Please try again."  + "\n");
				break;
			}
			
		}
		System.out.println("The program had now been closed.");
		input.close();
	}

}
