/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 1/31/2022 */

package labs.lab2;

import java.util.Scanner;

public class PrintXbyX {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the side length of your square.");
		int sideLength = Integer.parseInt(input.nextLine());
		for(int i = 1; i <= sideLength; i++)
		{
			System.out.println();
			for(int k = 1; k <= sideLength; k++)
			{
				System.out.print("X ");
			}
		}
		input.close();
	}

}
