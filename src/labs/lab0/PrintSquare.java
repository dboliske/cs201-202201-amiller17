/* 
 * package lab.lab0
 * public class PrintSquare
 * public static void main string args
 * String called square with value of "[]"
 * print value of square to a line
 * 
 * */
package labs.lab0;

public class PrintSquare {

	public static void main(String[] args) {
		
		String square = "[]";
		
		System.out.println(square);
	}

}

/* I wanted to have fun with this assignment, and find a way to print a square symbol to the console.
 * Thus, I searched a chart of unicode character codes for a symbol that looked like a square. 
 * I then use this character code, \u2BC0 in this case, as the value of my character.
 * Testing the print statement, this solution didn't work. 
 * CP1252, the standard text file encoding doesn't read the value properly.
 * While switching the text file encoding to UTF-8 worked, I didn't want to make anyone else do that.
 * Instead, I just made a string with two square brackets for an approximation of a square.*/