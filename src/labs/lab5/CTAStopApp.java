/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 3/7//2022 */

package labs.lab5;

import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class CTAStopApp {
	
	public static void main(String[] args) {
		
	}
	
	public CTAStation readFile(String filename) {
		int i = 1;
		File CTAStops = new File("src/labs/lab5/CTAStops.csv");
		try {
			Scanner fileRead = new Scanner(CTAStops);
			while(fileRead.hasNextLine()) {
				i++;
				String fileIn = fileRead.nextLine();
				String[] station = fileIn.split(",");
				String stationName = "Station" + i;
				CTAStation stationName = new CTAStation(station[0], Double.parseDouble(station[1]), Double.parseDouble(station[2]), station[3], Boolean.parseBoolean(station[4]), Boolean.parseBoolean(station[5]));
			}
			fileRead.close();
		}
		catch(Exception e) {
			System.out.println("File Not Found.");
		}
	}
	
	public void menu(CTAStation stations) {
		
	}
	
	public void displayStationNames(CTAStation stations) {
		
	}
	
	public void displayWheelchair(Scanner input, CTAStation stations) {
		Scanner userIn = new Scanner(System.in);
		System.out.println("Is wheelchair accessibility necessary? Y or N.");
		String userYN = userIn.nextLine();
		boolean validYN = false;
		while(!validYN) {
			switch(userYN.toLowerCase()) {
				case "y":
				{
					
					validYN = true;
				}
				case "n":
				{
					
					validYN = true;
				}
				default:
				{
					System.out.println("Enter a 'y' or a 'n' for your answer.");
				}
			}
		}
		userIn.close();
			
	}
	
	public void displayNearest(Scanner input, CTAStation stations) {
		Scanner userIn = new Scanner(System.in);
		System.out.println("Enter a latitude and then a longitude.");
		String userLatIn = userIn.nextLine();
		String userLngIn = userIn.nextLine();
		double userLat = Double.parseDouble(userLatIn);
		double userLng = Double.parseDouble(userLngIn);
		userIn.close();
	}
}