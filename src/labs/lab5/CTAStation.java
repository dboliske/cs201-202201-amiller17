/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 3/7//2022 */

package labs.lab5;

public class CTAStation extends GeoLocation {
	
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() {
		super();
		this.name = "Clinton";
		this.location = "elevated";
		this.wheelchair = true;
		this.open = true;
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public String toString() {
		if(wheelchair == true && open == true) {
			return "The " + location + " station " + name + "at latitude " + super.getLat() + " and longitude " + super.getLng() + "is wheelchair accessible and open.";
		}
		else if(wheelchair == false && open == true) {
			return "The " + location + " station " + name + "at latitude " + super.getLat() + " and longitude " + super.getLng() + "is not wheelchair accessible but is open.";
		}
		else if(wheelchair == true && open == false) {
			return "The " + location + " station " + name + "at latitude " + super.getLat() + " and longitude " + super.getLng() + "is wheelchair accessible but is not open.";
		}
		return "The " + location + " station " + name + "at latitude " + super.getLat() + " and longitude " + super.getLng() + "is not wheelchair accessible or open.";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!super.equals(obj)) {
			return false;
		}
		else if(!(obj instanceof CTAStation)) {
			return false;
		}
		CTAStation a = (CTAStation) obj;
		if (!this.name.equals(a.getName()) || !this.location.equals(a.getLocation()) || this.wheelchair!= a.hasWheelchair() || this.open != a.isOpen()) {
			return false;
		}
		return true;
	}
}
