/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 3/7//2022 */

package labs.lab5;

public class GeoLocation {
	private double lat;
	private double lng;
	
	public GeoLocation() {
		lat = 41.885678;
		lng = -87.641782;
	}
	
	public GeoLocation(double NS, double EW) {
		lat = NS;
		lng = EW;
	}
	
	public void setLat (double lat) {
			this.lat = lat;
	}
	
	public void setLng (double lng) {
			this.lng = lng;
	}
	
	public double getLat() {
		return lat;
	}
	public double getLng() {
		return lng;
	}
	public String toString() {
		return "(" + lat + ", " + lng + ")";
	}
	public boolean validLat(GeoLocation lat) {
		if (this.lat >= -90.0 && this.lat <=90.0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean validLng(GeoLocation lng) {
		if (this.lng >=-180.0 && this.lng <= 180.0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean equals(GeoLocation g) {
		if (this.lat != g.getLat()) {
			return false;
		}
		else if(this.lng != g.getLng()) {
			return false;
		}
		
		return true;
	}
	
	public double calcDistance(GeoLocation g) {
		return Math.sqrt(Math.pow(this.lat - g.getLat(), 2)+Math.pow(this.lng -g.getLng(), 2));
	}
	
	public double calcDistance(double lat, double lng) {
		return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
	}
	
}
