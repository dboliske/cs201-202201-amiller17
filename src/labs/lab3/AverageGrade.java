/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 2/9/2022 */

package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class AverageGrade {

	public static void main(String[] args) throws IOException {
		double gradeIn;
		double gradeTotal = 0;
		int numGrades = 0;
		File grades = new File("src/labs/lab3/grades.csv");
		Scanner input = new Scanner(grades);
		while(input.hasNextLine()) {
			String lineIn = input.nextLine();
			String[] nameGrade = lineIn.split(",");
			gradeIn = Double.parseDouble(nameGrade[1]);
			gradeTotal = gradeTotal + gradeIn;
			numGrades++;
			}
		if(numGrades==0) {
			System.out.println("No grades entered.");
		}
		else {
			System.out.println("The grade average is equal to " + (gradeTotal/numGrades) + ".");
		}
		input.close();
	}

}
