/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 2/9/2022 */

package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ArrayToFile {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean done = false;
		double[] values = new double[0];
		int num = 0;
		while(!done) {
			System.out.println("Enter a number. If done entering numbers, enter 'Done' to the console.");
			String userIn = input.nextLine();
			switch(userIn.toLowerCase()) {
			case "done":
			{
				done = true;
				break;
			}
			default:
			{
				num++;
				double[] addSize = new double[values.length + 1];
				for (int i=0; i<values.length; i++) {
					addSize[i] = values[i];
				}
				values = addSize;
				values[num-1] = Double.parseDouble(userIn);
				addSize = null;
				break;
			}
			}
		}
		if (num==0) {
			System.out.println("No numbers added. File will not be made.");
		}
		else {
			System.out.println("Enter the name for your file.");
			String userFile = input.nextLine();
			String fileName = "src/labs/lab3/" + userFile + ".txt";
			try {
				FileWriter outFile = new FileWriter(fileName);
				for( int k=0; k<num; k++) {
					outFile.write(values[k]+ "\n");
				}
				outFile.flush();
				outFile.close();
				System.out.println("The file " + userFile + ".txt has been made.");
			}
			catch(IOException e) {
				System.out.println("No file made.");
		}
	}
input.close();
}
}
