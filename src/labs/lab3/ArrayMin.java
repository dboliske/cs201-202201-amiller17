/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 2/9/2022 */

package labs.lab3;

public class ArrayMin {

	public static void main(String[] args) {
		int[] values = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		int min = values[0];
		for(int i=1; i<values.length; i++) {
			if(values[i]<min) {
				min = values[i];
			}
			else {}
		}
		System.out.println("The minimum of the array is " + min + ".");
	}

}
