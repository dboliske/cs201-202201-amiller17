/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/4/2022 */

package labs.lab7;

public class InsertionSort {

	public static String[] sort(String[] array) {
		for( int j=1; j<array.length; j++) {
			int i = j;
			while(i>0 && array[i].compareTo(array[i-1])<0) {
				String temp = array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				i--;
			}
		}
		return array;
	}
	
	public static void main(String[] args) {
		String[] things = {"cat", "fat", "dog", "apple", "bat", "egg"};
		things = sort(things);
		for(String l : things) {
			System.out.print(l + " ");
		}
	}

}
