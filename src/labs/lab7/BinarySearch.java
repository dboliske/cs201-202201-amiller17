/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/4/2022 */

package labs.lab7;

import java.util.Scanner;

public class BinarySearch {
	
	public static int search(String[] array, int left, int right, String val) {
		int middle = (left + right)/2;
		if(right >= 0 && left <= array.length - 1 && right >= left) {
			if(array[middle].equalsIgnoreCase(val)) {
				return middle;
			}
			else if(array[middle].compareToIgnoreCase(val)>0) {
				return search(array, left, middle - 1, val);
			}
			else if(array[middle].compareToIgnoreCase(val)<0) {
				return search(array, middle + 1, right, val);
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		String[] programs = {"c", "html", "java", "python", "ruby", "scala"};
		Scanner input = new Scanner(System.in);
		System.out.print("Search term: ");
		String userIn = input.nextLine();
		int position = search(programs, 0, programs.length, userIn);
		if(position !=-1) {
			System.out.println(userIn + " has an index of " + position + ".");
		}
		else {
			System.out.println(userIn + " not found.");
		}
		input.close();
	}

}
