/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 2/21/2022 */

package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		PhoneNumber phone1 = new PhoneNumber();
		PhoneNumber phone2 = new PhoneNumber("001", "708", "3418923");
		System.out.println(phone1.equals(phone2));
		System.out.println("The phone number of phone 1 is " + phone1.toString() + ".");
		System.out.println("The phone number of phone 2 is " + phone2.toString() + ".");
		phone1.setAreaCode("708");
		System.out.println("The phone number of phone 1 is " + phone1.toString() + ".");
		phone2.setNumber("0123456");
		System.out.println("The phone number of phone 2 is " + phone2.toString() + ".");
		System.out.println(phone1.equals(phone2));
	}

}
