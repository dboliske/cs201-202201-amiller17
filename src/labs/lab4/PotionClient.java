/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 2/21/2022 */

package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		Potion num1 = new Potion();
		Potion num2 = new Potion("Attack", 4);
		System.out.println(num1.toString());
		System.out.println(num2.toString());
		System.out.println(num1.equals(num2));
		num1.setName("Attack");
		System.out.println(num1.toString());
		System.out.println(num2.toString());
		System.out.println(num1.equals(num2));
		num1.setStrength(4);
		System.out.println(num1.toString());
		System.out.println(num2.toString());
		System.out.println(num1.equals(num2));
	}

}
