/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 2/21/2022 */

package labs.lab4;

public class Potion {
	private String name;
	private double strength;
	
	public Potion() {
		name = "Health";
		strength = 2.5;
	}
	
	public Potion(String type, double val) {
		name = type;
		strength = val;
	}
	
	public void setName(String type) {
		this.name = type;
	}
	
	public void setStrength(double val) {
		this.strength = val;
	}
	
	public String getName() {
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	public String toString() {
		return "The potion is a(n) " + name + " potion of strength " + strength + ".";
	}
	
	public boolean validStrength(double val) {
		if( this.strength >=0 && this.strength <= 10) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean equals(Potion nQual) {
		if(this.name != nQual.getName()) {
			return false;
		}
		else if(this.strength != nQual.getStrength()) {
			return false;
		}
		else {
			return true;
		}
	}
}
