/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 2/21/2022 */

package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		GeoLocation l1 = new GeoLocation();
		GeoLocation l2 = new GeoLocation(21.3, 33.1);
		System.out.println("The latitude of the default instance is " + l1.getLat() +
				" and the longitude is " + l1.getLng() + ".");
		System.out.println("The latitude of the non-defualt instance is " + l2.getLat() +
				" and the longitude is " + l2.getLng() + ".");
	}

}
