/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 2/21/2022 */

package labs.lab4;

public class PhoneNumber {
	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() {
		countryCode = "001";
		areaCode = "312";
		number = "0123456";
	}
	
	public PhoneNumber(String cC, String aC, String n) {
		countryCode = cC;
		areaCode = aC;
		number = n;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setCountryCode(String cC) {
		this.countryCode = cC;
	}
	
	public void setAreaCode(String aC) {
		this.areaCode = aC;
	}
	
	public void setNumber(String n) {
		this.number = n;
	}
	
	public String toString() {
		return countryCode + areaCode + number;
	}
	
	public boolean validAreaCode(PhoneNumber areaCode) {
		if(this.areaCode.length() == 3) {
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean validNumber(PhoneNumber number) {
		if(this.number.length() == 7) {
					return true;
		}
		else {
			return false;
		}
	}
	
	public boolean equals(PhoneNumber phoneNum) {
		if(this.countryCode != phoneNum.getCountryCode()) {
			return false;
		}
		else if(this.areaCode != phoneNum.getAreaCode()) {
			return false;
		}
		else if(this.number != phoneNum.getNumber()) {
			return false;
		}
		else {
			return true;
		}
		
	}
}
