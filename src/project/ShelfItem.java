/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/29/2022 */
//Shelf item class, for items without a necessary expiration date and no age limit. Can take in a name and a price.

package project;

public class ShelfItem {
	
	private String itemName;
	private double itemPrice;
	
	public ShelfItem() {
		itemName = "Placeholder";
		itemPrice = 0.00;
	}
	
	public ShelfItem(String itemName, double itemPrice) {
		this.itemName = itemName;
		this.itemPrice = itemPrice;
	}
	
	public String getName() {
		return itemName;
	}
	
	public double getPrice() {
		return itemPrice;
	}
	
	public void setName(String itemName) {
		this.itemName = itemName;
	}
	
	public void setPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	public boolean validPrice(double itemPrice) {
		if(itemPrice >=0.00) {
			return true;
		}
		return false;
	}
	public String toString() {
		return "Item: " + itemName + "\n" + "Price: " + itemPrice + "\n";
	}
	
	public boolean equals(Object item) {
		if(!(item instanceof ShelfItem)) {
			return false;
		}
		ShelfItem check = (ShelfItem) item;
		if(this.itemName != check.getName()) {
			return false;
		}
		else if(this.itemPrice != check.getPrice()) {
			return false;
		}
		return true;
	}
	
	public String toCSV() {
		return "Shelf" + "," + itemName + "," + itemPrice + "\n";
	}
}
