/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/29/2022 */

package project;

import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.FileWriter;

public class StoreApplication {
	//main: Main application, will send ArrayLists and scanner to other methods.
	public static void main(String[] args) {
		boolean done = false; //Determines whether or not the user is done.
		ArrayList<ShelfItem> stock = new ArrayList<ShelfItem>(); //ArrayList containing the stock of the store.
		ArrayList<ShelfItem> cart = new ArrayList<ShelfItem>();	//ArrayList containing the cart of a user.
		Scanner userIn = new Scanner(System.in); //Scanner allowing inputs from the user. Will be used throughout the program.
		while(!done) {
			System.out.println(printMenu());
			String menuOpt = userIn.nextLine(); //String that determines which option from the menu has been picked.
			switch(menuOpt) {
				case "1": case "1.":
				{
					importStock(stock, userIn);
					break;
				}
				case "2": case "2.":
				{
					exportStock(stock, userIn);
					break;
				}
				case "3": case "3.":
				{
					addStock(stock, userIn);
					break;
				}
				case "4": case "4.":
				{
					addCart(stock, cart, userIn);
					break;
				}
				case "5.": case "5":
				{
					removeCart(stock, cart, userIn);
					break;
				}
				case "6.": case "6":
				{
					searchStock(stock, userIn);
					break;
				}
				case "7": case "7.":
				{
					displayStock(stock);
					break;
				}
				case "8": case "8.":
				{
					displayCart(cart);
					break;
				}
				case "9": case "9.":
				{
					validMod(stock, cart, userIn);
					break;
				}
				case "10": case "10.":
				{
					checkOut(stock, cart, userIn);
					break;
				}
				case "11": case "11.":
				{
					done = exitProgram(stock, userIn);
					break;
				}
				default:
				{
					System.out.println("Valid option not entered. Returning to menu.");
					break;
				}
			}
		}
		System.out.println("Program ended.");
		userIn.close();
	}
	
	//printMenu: Contains the menu string.
	public static String printMenu() {
		String menu = "1. Import previous stock. \n"; //String that stores what the menu looks like.
		menu += "2. Export current stock. \n";
		menu += "3. Add to stock. \n";
		menu += "4. Add to cart. \n";
		menu += "5. Remove from cart. \n";
		menu += "6. Search stock for item. \n";
		menu += "7. Display current stock. \n";
		menu += "8. Display current cart. \n";
		menu += "9. Modify items. \n";
		menu += "10. Check out items. \n";
		menu += "11. Exit Program.";
		return menu;
	}
	
	//importStock: Reads in files. Divides the input string from the file at commas, then determines type of item, then the value of each of the characteristics, then adds to the stock.
	public static void importStock(ArrayList<ShelfItem> stock, Scanner userIn) {
		try {
			ArrayList<ShelfItem> savePoint = stock; //Copy of current stock in case of error while loading file.
			System.out.println("Enter the file path of your input file.");
			String inputFile = userIn.nextLine(); //Stores the input file name.
			File fileIn = new File(inputFile); //Connects to input file.
			Scanner fileRead = new Scanner(fileIn); //For reading in inputs.
			while(fileRead.hasNextLine()) {
				String fileLine = fileRead.nextLine(); //Stores the csv line.
				String[] lineBreak = fileLine.split(","); //Array that stores each individual string, separated at the comma.
				switch(lineBreak[0]) { //lineBreak[0] stores the type of the item. Shelf, Produce, Restricted.
					case "Shelf": case "Produce": case "Restricted":{
						try {
							double price = Double.parseDouble(lineBreak[2]); //The cost of the item.
							if(price<=0) {
								System.out.println("Potentially corrupted file detected. Check file. Stock reset to before file lead. Returning to menu");
								stock = savePoint;
								return;
							}
							switch(lineBreak[0]) {
								case "Shelf":
									ShelfItem shelfGood = new ShelfItem(lineBreak[0], Double.parseDouble(lineBreak[1]));
									stock.add(shelfGood);
									break;
								case "Produce":
									try {
										String[] dateBreak = lineBreak[3].split("/"); //Separates the date at the forward slashes.
										if(Integer.parseInt(dateBreak[0])<=0 || Integer.parseInt(dateBreak[0])>31) {
											System.out.println("Potentially corrupted file detected. Check file. Stock reset to before file lead. Returning to menu");
											stock = savePoint;
											return;
										}
										if(Integer.parseInt(dateBreak[1])<=0 || Integer.parseInt(dateBreak[1])>12) {
											System.out.println("Potentially corrupted file detected. Check file. Stock reset to before file lead. Returning to menu");
											stock = savePoint;
											return;
										}
										if(Integer.parseInt(dateBreak[2])<1970) {
											System.out.println("Potentially corrupted file detected. Check file. Stock reset to before file lead. Returning to menu");
											stock = savePoint;
											return;
										}
										ProduceItem produceGood = new ProduceItem(lineBreak[1], Double.parseDouble(lineBreak[2]), Integer.parseInt(dateBreak[0]), Integer.parseInt(dateBreak[1]), Integer.parseInt(dateBreak[3])); //Makes the new produce item.
										stock.add(produceGood);
										break;
									}
									catch( Exception e) {
										System.out.println("Potentially corrupted file detected. Check file. Stock reset to before file lead. Returning to menu");
										stock = savePoint;
										return;
									}
								case "Restricted":
									try {
										if(Integer.parseInt(lineBreak[3])<=0) {
											System.out.println("Potentially corrupted file detected. Check file. Stock reset to before file lead. Returning to menu");
											stock = savePoint;
											return;
										}
										RestrictedItem restrictedGood = new RestrictedItem(lineBreak[1], Double.parseDouble(lineBreak[2]), Integer.parseInt(lineBreak[3])); //Makes the new restricted item.
										stock.add(restrictedGood);
										break;
									}
									catch( Exception e) {
										System.out.println("Potentially corrupted file detected. Check file. Stock reset to before file lead. Returning to menu");
										stock = savePoint;
										return;
									}
							}
						}
						catch (Exception e) {
							System.out.println("Potentially corrupted file detected. Check file. Stock reset to before file lead. Returning to menu");
							stock = savePoint;
							return;
						}
					}
				}
			}
			fileRead.close();
		}
		catch(Exception e) {
			System.out.println("Error occurred reading in file. Returning to menu.");
		}
	}
	
	//exportStock: Writes to files. Writes information as a csv line.
	public static boolean exportStock(ArrayList<ShelfItem> stock, Scanner userIn) {
		System.out.println("Enter the file path of your export file.");
		String exportFile = userIn.nextLine(); //Stores the name of the output file.
		try {
			FileWriter fileOut = new FileWriter(exportFile); //Allows for writing to output file.
			for(ShelfItem s: stock) {
				fileOut.write(s.toCSV());
			}
			fileOut.close();
			System.out.println("Stock saved.");
			return true;
		} 
		catch(Exception e) {
			System.out.println("Error writing to file. Please double check file location.");
		}
		return false;
	}
	
	//addStock: Allows the user to manually add a number of stock. Asks for type of item, then number of items to be added, then the name and price of the item, before sending to other methods if needed.
	public static void addStock(ArrayList<ShelfItem> stock, Scanner userIn) {
		System.out.println("Enter the number of the type of item(s). (0 to exit.)" + "\n" + "1. Shelf item." + "\n" + "2. Produce Item." + "\n" + "3. Restricted Item.");
		int itemType = validType(userIn.nextLine(), userIn); //Stores the item type. Shelf, produce, restricted.
		switch(itemType) {
			case 0:
				System.out.println("Returning to menu.");
				return;
			case 1: case 2: case 3:{
				System.out.println("Enter the number of items. (0 to exit.)");
				int itemNumber = validNum(userIn.nextLine(), userIn); //Stores the number of items to be added.
				switch(itemNumber) {
				case 0:
					System.out.println("Returning to menu.");
					return;
				default:
					System.out.println("Enter the name of the item(s).");
					String itemName = userIn.nextLine(); //Stores the item name.
					System.out.println("Enter the price of the item(s). (0 to exit.)");
					double itemPrice = validPrice(userIn.nextLine(), userIn); //Stores the price of the items to be added.
					if(itemPrice == 0) {
						System.out.println("Returning to menu.");
						return;
					}
					else {
						switch(itemType) {
							case 1:
								addShelf(stock, itemName, itemPrice, itemNumber);
								break;
							case 2:
								addProduce(stock, itemName, itemPrice, itemNumber, userIn);
								break;
							case 3:
								addRestricted(stock, itemName, itemPrice, itemNumber, userIn);
								break;
						}
					}
				}
				break;
			}
		}
	}


	//addCart: Removes an instance of the requested item from the stock, determined by the input name, and adds it to the cart.
	public static void addCart(ArrayList<ShelfItem> stock, ArrayList<ShelfItem> cart, Scanner userIn) {
		if(stock.size()==0) {
			System.out.println("No items in stock. Returning to menu.");
			return;
		}
		System.out.println("Enter the name of the item you wish to add to the cart.");
		String inName = userIn.nextLine();
		int numFound = countMatch(stock, inName);
		if(numFound == 0) {
			System.out.println("Item not found in stock. Returning to menu.");
			return;
		}
		ShelfItem temp = returnMatch(stock, inName);
		cart.add(temp);
		stock.removeIf(item -> item == temp);
		for(int i = 1; i < numFound; i++) {
			stock.add(temp);
		}
	}
	
	//removeCart: Removes an instance of the requested item from the cart, determined by input name, and adds it to the stock.
	public static void removeCart(ArrayList<ShelfItem> stock, ArrayList<ShelfItem> cart, Scanner userIn) {
		if(cart.size()==0) {
			System.out.println("No items in cart. Returning to menu.");
			return;
		}
		System.out.println("Enter the name of the item you wish to remove from the cart.");
		String inName = userIn.nextLine();
		int numFound = countMatch(cart, inName);
		if(numFound == 0) {
			System.out.println("Item not found in cart. Returning to menu.");
			return;
		}
		ShelfItem temp = returnMatch(cart, inName);
		stock.add(temp);
		cart.removeIf(item -> item == temp);
		for(int i = 1; i < numFound; i++) {
			cart.add(temp);
		}
	}
	
	//searchStock: Searches the stock for how many items have the input name.
	public static void searchStock(ArrayList<ShelfItem> stock, Scanner userIn) {
		System.out.println("Enter the name of the item which you wish to search for. Exact wording/spelling matters, capitalization does not.");
		String inName = userIn.nextLine(); //The name of the item to search for.
		int numberFound = 0; //Stores the number of items with the desired name have been found.
		numberFound = countMatch(stock, inName);
		if(numberFound != 0) {
			System.out.println(numberFound + " item(s) with name \"" + inName + "\" found in the stock.");
		}
		else if(stock.size()==0) {
			System.out.println("No stock to search through.");
		}
		else{
			System.out.println("Item not found in stock. Double check the name of the item.");
		}
	}
	
	//displayStock: Prints the stock to the console.
	public static void displayStock(ArrayList<ShelfItem> stock) {
		System.out.println("Current Stock: \n");
		for(ShelfItem s: stock) {
			System.out.println(s.toString());
		}
	}
	
	//displayCart: Prints the cart to the console.
	public static void displayCart(ArrayList<ShelfItem> cart) {
		System.out.println("Current Cart: \n");
		for(ShelfItem s: cart) {
			System.out.println(s.toString());
		}
	}
	
	//validMod: Asks for type and name of item, then the characteristic to modify. Checks validity of entered values, before sending to the respective modChar.
	public static void validMod(ArrayList<ShelfItem> stock, ArrayList<ShelfItem> cart, Scanner userIn) 
	{ArrayList<ArrayList<ShelfItem>> listArray = new ArrayList<ArrayList<ShelfItem>>(); //Stores both stock and cart to reduce code.
		listArray.add(stock);
		listArray.add(cart);
		System.out.println("Enter the number of the type of item(s) to modify. (0 to exit.)" + "\n" + "1. Shelf item." + "\n" + "2. Produce Item." + "\n" + "3. Restricted Item.");
		int itemType = validType(userIn.nextLine(), userIn); //Stores the item type. Shelf, produce, restricted.
		switch(itemType) {
			case 0:
				System.out.println("Returning to menu.");
				return;
			case 1: case 2: case 3:{
				System.out.println("Enter the name of the item(s).");
				String inName = userIn.nextLine(); //Stores the item name to search for.
				System.out.println("Enter the number of the characteristic. (0 to exit.) \n 1. Item name. \n 2. Item price. \n 3. Expiration date. \n 4. Age limit.");
				int itemChar = validChar(userIn.nextLine(), userIn); //Stores the item characteristic.
				if(itemChar == 3 && (itemType == 1 || itemType == 3)) {
					System.out.println("Entered characteristic not valid for the entered item type. Returning to menu.");
					return;
				}
				else if(itemChar == 4 && (itemType == 1 || itemType == 2)) {
					System.out.println("Entered characteristic not valid for the entered item type. Returning to menu.");
					return;
				}
				else if(itemChar == 0) {
					System.out.println("Returning to menu.");
					return;
				}
				else {
					switch(itemChar) {
						case 1:
							System.out.println("Enter the new name for the item(s).");
							String newName = userIn.nextLine(); //Stores the new name.
							modName(listArray, inName, newName);
							break;
						case 2:
							System.out.println("Enter the new price of the item(s). (0 to exit.)");
							double newPrice = validPrice(userIn.nextLine(), userIn); //Stores the new price.
							if(newPrice == 0) {
								System.out.println("Returning to menu.");
								return;
							}
							else {
								modPrice(listArray, inName, newPrice);
							}
							break;
						case 3:
							System.out.println("Enter the integer value of the new day of expiration for the item(s). (0 to exit.)");
							int newDay = validDay(userIn.nextLine(), userIn); //Stores the day of expiration of the items to be added.
							switch(newDay) {
								case 0:
									System.out.println("Returning to menu");
									return;
								default:
									System.out.println("Enter the integer value of the new month of expiration for the item(s). (0 to exit.)");
									int newMonth = validMonth(userIn.nextLine(), userIn); //Stores the month of expiration of the items to be added.
									switch(newMonth) {
										case 0:
											System.out.println("Returning to menu");
											return;
										default:
											System.out.println("Enter the integer value of the new year of expiration for the item(s). (0 to exit.)");
											int newYear = validYear(userIn.nextLine(), userIn); //Stores the month of expiration of the items to be added.
											switch(newYear) {
										 		case 0:
										 			System.out.println("Returning to menu");
										 			return;
										 		default:
										 			modExpiration(listArray, inName, newDay, newMonth, newYear);
										 			break;
											}
											break;
									}
									break;
							}
							break;
						case 4:
							System.out.println("Enter the new age limit of the item(s). (0 to exit.)");
							int newAge = validAge(userIn.nextLine(), userIn);
							switch(newAge) {
								case 0:
									System.out.println("Returning to menu");
									return;
								default:
									modAge(listArray, inName, newAge);
									break;
							}
							break;
					}
				}
				break;
			}
		}
	}
	
	//checkOut: Checks for restricted items. If found, and purchaser is under the maximum age limit, removes all restricted items. Prompts for payment, if payment is denied returns to menu. Else, clears cart.
	public static void checkOut(ArrayList<ShelfItem> stock, ArrayList<ShelfItem> cart, Scanner userIn) {
		if(cart.size()==0) {
			System.out.println("No items to check out. Returning to menu.");
			return;
		}
		else {
			double cost = 0; //Stores the cost of the cart.
			int ageLim = 0; //Stores the current maximum age limit.
			boolean restItem = false; //Stores whether or not there are restricted items.
			for(ShelfItem c: cart) {
				if(c instanceof RestrictedItem) {
					restItem = true;
					RestrictedItem r = (RestrictedItem) c;
					if(r.getLimit()>ageLim)
					{
						ageLim = r.getLimit();
					}
				}
			}
			if(restItem == true) {
				System.out.println("Is the purchaser is at least " + ageLim + " years old? Yes or no.");
				String yn = userIn.nextLine();
				switch(yn.toLowerCase()) {
					case "y": case "yes":{
						for(ShelfItem c: cart) {
							cost = cost + c.getPrice();
						}
						break;
					}
					case "n": case "no":{
						cart.removeIf(item -> (item instanceof RestrictedItem));
						System.out.println("Restricted items removed from cart.");
						displayCart(cart);
						if(cart.size()!=0) {
							for(ShelfItem c: cart) {
								cost = cost + c.getPrice();
							}
						}
						break;
					}
					default:
						System.out.println("Valid entry not detected. Returning to menu.");
						return;
				}
			}
			else {
				for(ShelfItem c: cart) {
					cost = cost + c.getPrice();
				}
			}
			if(cart.size()!=0) {
				System.out.println("The cost is $" + cost +  ". Is payment accepted?");
				String yn = userIn.nextLine();
				switch(yn.toLowerCase()) {
					case "y": case "yes":{
						System.out.println("Emptying cart.");
						cart = null;
						break;
					}
					case "n": case "no":{
						System.out.println("Returning to menu.");
						break;
					}
				}
			}
			else {
				System.out.println("No items to check out. Returning to menu.");
				return;
			}
		}
	}

	//exitProgram: Exits the program. Asks if the user wishes to export current stock first.
	public static boolean exitProgram(ArrayList<ShelfItem> stock, Scanner userIn) {
		System.out.println("Save stock? Yes or No.");
		String userYN = userIn.nextLine();
		switch(userYN.toLowerCase()) {
			case "yes": case "y":
			{
				if(exportStock(stock, userIn)==true) {
					return true;
				}
				else {
					return false;
				}
				
			}
			case "no": case "n":
			{
				System.out.println("Stock not saved.");
				return true;
			}
			default:
			{
				System.out.println("No valid entry entered. Returning to menu.");
				return false;
			}
		}
	}
	
	//returnMatch: Returns the first case of an item with the entered name in an ArrayList. If there is no match, returns null.
	public static ShelfItem returnMatch(ArrayList<ShelfItem> list, String inName) {
		boolean inList = true; //Stores whether is in the list.
		while(inList) {
			for(ShelfItem item: list) {
				if(item.getName().equalsIgnoreCase(inName)) {
					return item;
				}
			}
			inList = false;
		}
		return null;
	}
	
	//countMatch: Counts how many of the desired item are in the stock/cart.
	public static int countMatch(ArrayList<ShelfItem> list, String inName) {
		int numFound = 0;
		for(ShelfItem item: list) {
			if(item.getName().equalsIgnoreCase(inName)) {
				numFound++;
			}
		}
		return numFound;
	}
	
	//removeMatch: Removes the desired item from the stock and cart.
	public static ArrayList<ShelfItem> removeMatch(ArrayList<ShelfItem> list, String inName){
		ArrayList<ShelfItem> aftRem = new ArrayList<ShelfItem>(); //Stores the new ArrayList to be returned.
		list.removeIf(item -> item.getName().equalsIgnoreCase(inName));
		aftRem = list;
		return aftRem;
	}
	
	//addMod: Adds back modified items to the stock and cart.
	public static ArrayList<ShelfItem> addMod(ArrayList<ShelfItem> list, ShelfItem item, int numFound){
		ArrayList<ShelfItem> temp = list;
		for(int i = 0; i<numFound; i++){
			list.add(item);
		}
		return temp;
	}

	//modName: Modifies the name of the desired item(s) in both stock and cart.
	public static void modName(ArrayList<ArrayList<ShelfItem>> listArray, String inName, String newName) {
		int numFound = 0;
		for(ArrayList<ShelfItem> list: listArray) {
			ShelfItem temp = returnMatch(list, inName); //Store old values of the item.
			if(temp != null) {
			numFound = countMatch(list, inName);
			list = removeMatch(list, inName);
			temp.setName(newName);
			list = addMod(list, temp, numFound);
			numFound = 0;
			}
		}
		
	}
	
	//modPrice: Modifies the price of the desired item(s) in b.oth stock and cart.
	public static void modPrice(ArrayList<ArrayList<ShelfItem>> listArray, String inName, double newPrice) {
		int numFound = 0;
		for(ArrayList<ShelfItem> list: listArray) {
			ShelfItem temp = returnMatch(list, inName); //Store old values of the item.
			if(temp != null) {
				numFound = countMatch(list, inName);
				list = removeMatch(list, inName);
				temp.setPrice(newPrice);
				list = addMod(list, temp, numFound);
				numFound = 0;
			}
		}
	}
	
	//modExpiration: Modifies the expiration date of the desired item(s) in both stock and cart.
	public static void modExpiration(ArrayList<ArrayList<ShelfItem>> listArray, String inName, int entDay, int entMonth, int entYear) {	
		int numFound = 0;
		for(ArrayList<ShelfItem> list: listArray) {
			ProduceItem temp = (ProduceItem) returnMatch(list, inName); //Store old values of the item.
			if(temp != null) {
				numFound = countMatch(list, inName);
				list = removeMatch(list, inName);
				temp.setDay(entDay);
				temp.setMonth(entMonth);
				temp.setYear(entYear);
				list = addMod(list, temp, numFound);
				numFound = 0;
			}
		}
	}
	
	//modAge: Modifies the age limit of the desired item(s) in both stock and cart.
	public static void modAge(ArrayList<ArrayList<ShelfItem>> listArray, String inName, int newLimit) {
		
		int numFound = 0;
		for(ArrayList<ShelfItem> list: listArray) {
			RestrictedItem temp = (RestrictedItem) returnMatch(list, inName); //Store old values of the item.
			if(temp != null) {
			numFound = countMatch(list, inName);
			list = removeMatch(list, inName);
			temp.setLimit(newLimit);
			list = addMod(list, temp, numFound);
			numFound = 0;
			}
		}
		
	}
	
	//validType: Determines whether the user entered a valid type.
	public static int validType(String userEnt, Scanner userIn) {
		int itemType = -1;
		try {
			itemType = Integer.parseInt(userEnt);
		}
		catch(Exception e) {
		}
		while(!(itemType >=0 && itemType <= 3)) {
			System.out.println("Valid number not entered. Please try again.");
			try {
				itemType = Integer.parseInt(userIn.nextLine());
			}
			catch(Exception e) {
			}
		}
		return itemType;
	}
	
	public static int validChar(String userEnt, Scanner userIn) {
		int itemChar = -1;
		try {
			itemChar = Integer.parseInt(userEnt);
		}
		catch(Exception e) {
		}
		while(!(itemChar >=0 && itemChar <=4)) {
			System.out.println("Valid number not entered. Please try again.");
			try {
				itemChar = Integer.parseInt(userIn.nextLine());
			}
			catch(Exception e) {
			}
		}
		return itemChar;
	}
	
	//validPrice: Determines whether the user entered a valid price.
	public static double validPrice(String userEnt, Scanner userIn) {
		double itemPrice = -1;
		try {
			itemPrice = Double.parseDouble(userEnt);
		}
		catch(Exception e) {
		}
		while(!(itemPrice >=0.00)) {
			System.out.println("Valid number not entered. Please try again.");
			try {
				itemPrice = Double.parseDouble(userIn.nextLine());
			}
			catch(Exception e) {
			}
			
		}
		return itemPrice;
	}
	
	//validDay: Determines whether the user entered a valid day.
	public static int validDay(String userEnt, Scanner userIn) {
		int itemDay = -1;
		try {
			itemDay = Integer.parseInt(userEnt);
		}
		catch(Exception e) {
		}
		while(!(itemDay >=0 && itemDay <=31)) {
			System.out.println("Valid number not entered. Please try again.");
			try {
				itemDay = Integer.parseInt(userIn.nextLine());
			}
			catch(Exception e) {
			}
		}
		return itemDay;
	}
	
	//validMonth: Determines whether the user entered a valid month.
	public static int validMonth(String userEnt, Scanner userIn) {
		int itemMonth = -1;
		try {
			itemMonth = Integer.parseInt(userEnt);
		}
		catch(Exception e) {
		}
		while(!(itemMonth >=0 && itemMonth <=12)) {
			System.out.println("Valid number not entered. Please try again.");
			try {
				itemMonth = Integer.parseInt(userIn.nextLine());
			}
			catch(Exception e) {
			}
		}
		return itemMonth;
	}
	
	//validYear: Determines whether the user entered a valid year.
	public static int validYear(String userEnt, Scanner userIn) {
		int itemYear = -1;
		try {
			itemYear = Integer.parseInt(userEnt);
		}
		catch(Exception e) {
		}
		while(!(itemYear>=1970)) {
			System.out.println("Valid number not entered. Please try again.");
			try {
				itemYear = Integer.parseInt(userIn.nextLine());
			}
			catch(Exception e) {
			}
		}
		return itemYear;
	}
	
	//validAge: Determines whether the user entered a valid age limit.
	public static int validAge(String userEnt, Scanner userIn) {
		int itemAge = -1;
		try {
			itemAge = Integer.parseInt(userEnt);
		}
		catch(Exception e) {
		}
		while(!(itemAge >=0)) {
			System.out.println("Valid number not entered. Please try again.");
			try {
				itemAge = Integer.parseInt(userIn.nextLine());
			}
			catch(Exception e) {
			}
		}
		return itemAge;
	}
	
	//validNum: Determines whether the user has entered a valid number of items.
	public static int validNum(String userEnt, Scanner userIn) {
		int numItem = -1;
		try {
			numItem = Integer.parseInt(userEnt);
		}
		catch(Exception e) {
		}
		while(!(numItem >=0)) {
			System.out.println("Valid number not entered. Please try again.");
			try {
				numItem = Integer.parseInt(userIn.nextLine());
			}
			catch(Exception e) {
			}
		}
		return numItem;
	}	

	//addShelf: Adds shelf item to stock.
	public static void addShelf(ArrayList<ShelfItem> stock, String itemName, double itemPrice, int itemNumber) {
		ShelfItem shelfGood = new ShelfItem(itemName, itemPrice); //The new shelf item to be added.
		for(int i = 1; i<=itemNumber; i++) {
			stock.add(shelfGood);
		}
	}
	
	//addProduce: Gathers rest of data for a produce item, then adds item(s) to stock.
	public static void addProduce(ArrayList<ShelfItem> stock, String itemName, double itemPrice, int itemNumber, Scanner userIn) {
		System.out.println("Enter the integer value of the day of expiration for the item(s). (0 to exit.)");
		int itemDay = validDay(userIn.nextLine(), userIn); //Stores the day of expiration of the items to be added.
		switch(itemDay) {
			case 0:
				System.out.println("Returning to menu");
				break;
			default:
				System.out.println("Enter the integer value of the month of expiration for the item(s). (0 to exit.)");
				int itemMonth = validMonth(userIn.nextLine(), userIn); //Stores the month of expiration of the items to be added.
				switch(itemMonth) {
					case 0:
						System.out.println("Returning to menu");
						break;
					default:
						System.out.println("Enter the integer value of the year of expiration for the item(s). (0 to exit.)");
						int itemYear = validYear(userIn.nextLine(), userIn); //Stores the month of expiration of the items to be added.
						switch(itemYear) {
					 		case 0:
					 			System.out.println("Returning to menu");
					 			break;
					 		default:
					 			ProduceItem produceGood = new ProduceItem(itemName, itemPrice, itemDay, itemMonth, itemYear); //The new produce item to be added.
					 			for(int i = 1; i<=itemNumber; i++) {
					 				stock.add(produceGood);
					 			}
					 			break;
						}
						break;
				}
				break;
		}
	}

	//addRestricted: Gathers rest of data for a restricted item, then adds item(s) to stock.
	public static void addRestricted(ArrayList<ShelfItem> stock, String itemName, double itemPrice, int itemNumber, Scanner userIn) {
		System.out.println("Enter the age limit for the item(s). (0 to exit.)");
		int itemAge = validAge(userIn.nextLine(), userIn); //Stores the age limit of the items to be added.
		switch(itemAge) {
			case 0:
				System.out.println("Returning to menu");
				break;
			default:
				RestrictedItem restrictedGood = new RestrictedItem(itemName, itemPrice, itemAge); //The new restricted item to be added.
				for(int i = 1; i<=itemNumber; i++) {
	 				stock.add(restrictedGood);
	 			}
				break;
		}
	}
	
}