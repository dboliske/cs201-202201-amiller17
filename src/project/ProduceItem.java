/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/29/2022 */
//Produce item class, for items that may expire. Can take in a name, price, and the day, month, and year of expiration.

package project;

public class ProduceItem extends ShelfItem{
	private int expirationDay;
	private int expirationMonth;
	private int expirationYear;
	public ProduceItem() {
		super();
		expirationDay = 1;
		expirationMonth = 1;
		expirationYear = 1970;
	}
	
	public ProduceItem(String itemName, double itemPrice, int expirationDay, int expirationMonth, int expirationYear) {
		super(itemName, itemPrice);
		this.expirationDay = expirationDay;
		this.expirationMonth = expirationMonth;
		this.expirationYear = expirationYear;
	}
	
	public int getDay() {
		return expirationDay;
	}
	
	public int getMonth() {
		return expirationMonth;
	}
	
	public int getYear() {
		return expirationYear;
	}
	
	public String getDate() {
		return expirationMonth + "/" + expirationDay + "/" + expirationYear;
	}
	
	public void setDay(int expirationDay) {
		this.expirationDay = expirationDay;
	}
	
	public void setMonth(int expirationMonth) {
		this.expirationMonth = expirationMonth;
	}
	
	public void setYear(int expirationYear) {
		this.expirationYear = expirationYear;
	}
	
	public boolean validDay(int expirationDay){
		if(expirationDay <=31 && expirationDay >=1) {
			return true;
		}
		return false;
	}
	
	public boolean validMonth(int expirationMonth) {
		if(expirationMonth >= 1 && expirationMonth <= 12) {
			return true;
		}
		return false;
	}
	
	public boolean validYear(int expirationYear) {
		if(expirationMonth >=  1970) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return super.toString()	+ this.getDate() + "\n \n";
	}
	
	@Override
	public boolean equals(Object item) {
		if(!(item instanceof ProduceItem)) {
			return false;
		}
		ProduceItem check = (ProduceItem) item;
		if(super.getName() != check.getName()) {
			return false;
		}
		else if(super.getPrice() != check.getPrice()) {
			return false;
		}
		else if(this.getDate() != check.getDate()) {
			return false;
		}
		return true;
	}

	@Override
	public String toCSV() {
		return "Produce," + super.getName() + "," + super.getPrice() + "," + this.getDate() + "\n";
	}
}
