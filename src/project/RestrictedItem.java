/*Alexander Miller
 * CS-201-01
 * Dylan Boliske
 * 4/29/2022 */
//Restricted item class, for items that require a minimum age to buy. Can take in a name, a price, and a minimum age.

package project;

public class RestrictedItem extends ShelfItem{
	
	private int ageLimit;
	
	public RestrictedItem() {
		super();
		ageLimit = 0;
	}
	
	public RestrictedItem(String itemName, double itemPrice, int ageLimit) {
		super(itemName, itemPrice);
		this.ageLimit = ageLimit;
	}
	
	public int getLimit() {
		return ageLimit;
	}
	
	public void setLimit(int ageLimit) {
		this.ageLimit = ageLimit;
	}
	
	public boolean validLimit(int ageLimit) {
		if(ageLimit >=0) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return super.toString() + "Age Limit: " + ageLimit + "\n \n";
	}
	
	@Override
	public  boolean equals(Object item) {
		if(!(item instanceof RestrictedItem)) {
			return false;
		}
		RestrictedItem check = (RestrictedItem) item;
		if(super.getName() != check.getName()) {
			return false;
		}
		else if(super.getPrice() != check.getPrice()) {
			return false;
		}
		else if(this.ageLimit != check.ageLimit) {
			return false;
		}
		return true;
	}
	@Override
	public String toCSV() {
		return "Restricted," + super.getName() + "," + super.getPrice() + "," + this.ageLimit + "\n";
	}
}
